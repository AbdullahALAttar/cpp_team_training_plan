# C++ team training plan

You learn about each topic from the suggested resources or any other resources you may like.

There should be a mini-quiz discussion after you complete each topic.

# Orientation Session
**Estimated Duration: 1 Day.**

* People
* Training Plan
* Servers
* Projects
* Memes (Culture)
* Workflow

# Smart Pointers
**Estimated Duration: 4 Hours.**
* [Leak Freedom by Herb Sutter](https://www.youtube.com/watch?v=JfmTagWcqoE)

Points to stress at: Ownership Semantics

# Move Semantics
**Estimated Duration: 1 Day.**

* [Move Semantics by Moustapha Saad](https://moustaphasaad.github.io/2017/07/14/move-semantics/)

# Tuples
**Estimated Duration: 4 Hours.**

* [std::tuple - cppreference](https://en.cppreference.com/w/cpp/utility/tuple)

# Templates
**Estimated Duration: 1 Day.**

* [Template Normal Programming - Part 1](https://www.youtube.com/watch?v=vwrXHznaYLA)
* [Template Normal Programming - Part 2](https://www.youtube.com/watch?v=VIz6xBvwYd8)

# Parallel Programming
**Estimated Duration: 2 Days.**

* std::thread
* OpenMP
* Consumer/Producer Pattern
* std::future, std::promise
* std::condition_variable

# Linux Command Line Basics
**Estimated Duration: 1 Day.**

* ls
* pwd
* cd
* cp
* mv
* rm
* man
* echo
* cat
* mkdir
* rmdir
* touch
* nano
* chmod
* chown
* sudo
* Piping
* Redirection

# Git
**Estimated Duration: 4 Hours.**

We'll have a nice, practical session.

Here's a [cheatsheet](https://github.com/Open-Source-Community/iusegit/blob/master/Cheatsheet.md) to refer to when in need.

# Dynamic/Static Linking
**Estimated Duration: 1 Day.**

**C++ Library Programming on Linux**

* [Part 1: Static Libraries](http://www.techytalk.info/c-cplusplus-library-programming-on-linux-part-one-static-libraries/)
* [Part 2: Dynamic Libraries](http://www.techytalk.info/c-cplusplus-library-programming-on-linux-part-two-dynamic-libraries/)
* [Part 3: Dynamic Libraries using POSIX API](http://www.techytalk.info/c-cplusplus-library-programming-on-linux-part-three-dynamic-libraries-using-posix-api/)

# Qmake/Make toolchains
**Estimated Duration: 4 Hours.**

* [Makefile](https://www.youtube.com/watch?v=_r7i5X0rXJk)
* [Qmake](https://doc.qt.io/qt-5/qmake-manual.html)

# Basics of Image Processing
**Estimated Duration: 1 Day.**


# Basics of Machine Learning
* [Basics](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi): 
  - Feedforward.
  - Backpropagation.
  - Loss functions.

**Estimated Duration: 1 Day.**

* [Convolutional neural networks](https://www.youtube.com/watch?v=ArPaAX_PhIs&list=PLkDaE6sCZn6Gl29AoE31iwdVwSG-KnDzF).

**Estimated Duration: 1 Day.**

* Recurrent neural networks
* [Andrew Ng list](https://www.youtube.com/watch?v=DejHQYAGb7Q&list=PLkDaE6sCZn6F6wUI9tvS_Gw1vaFAx6rd6)
* [Recurrent Neural Networks Tutorial, Part 1](http://www.wildml.com/2015/09/recurrent-neural-networks-tutorial-part-1-introduction-to-rnns/)
  - Basic structure.
  - LSTM.
  - Language models.
  - GRU.
  - Attention.
  - back stitch.
  - tdnn.

**Estimated Duration: 1 Day and 4 Hours.**

# Graduation Project :smile:
**Estimated Duration: 3 Days.**
* Implement a Red Black Tree
* Use git to synchronize code among yourselves
* Write tests
* Write Continuous Integration Scripts
* Write Continuous Delivery Scripts